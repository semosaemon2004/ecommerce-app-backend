const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const signupRoutes = require("./routes/signup");
const loginRoutes = require("./routes/login");
const verificationRoutes = require("./routes/verification");

const app = express();
const PORT = process.env.PORT || 3000;

app.use(bodyParser.json());

// Connect to MongoDB
mongoose
  .connect(
    "mongodb+srv://waseem:123456waseem@cluster0.foj04zv.mongodb.net/?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  )
  .then(() => {
    console.log("Connected to MongoDB");
  })
  .catch((err) => {
    console.error("MongoDB connection error:", err);
  });

// Use the signup and login routes
app.use("/signup", signupRoutes);
app.use("/login", loginRoutes);
app.use("/verify", verificationRoutes);

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
