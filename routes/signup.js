const express = require("express");
const bcrypt = require("bcrypt");
const nodemailer = require("nodemailer");
const router = express.Router();

const User = require("../models/user");
const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "mwaflutterdeveloper@gmail.com",
    pass: "lbjiubhlpdvxhnrt",
  },
});

router.post("/", async (req, res) => {
  try {
    const { username, email, phoneNumber, password } = req.body;

    const existingUser = await User.findOne({ email });
    if (existingUser) {
      return res.status(409).json({ message: "User already exists" });
    }

    const hashedPassword = await bcrypt.hash(password, 10);

    const verificationCode = generateVerificationCode(); // Generate a unique code

    const newUser = new User({
      username,
      email,
      phoneNumber,
      password: hashedPassword,
      verificationCode, // Store the code in the user's record
      isEmailVerified: false, // Set email verification status to false
    });

    await newUser.save();

    // Send a verification email with the verification code
    const mailOptions = {
      from: "mwaflutterdeveloper@gmail.com",
      to: email,
      subject: "Verify Your Email",
      text: `Your verification code is: ${verificationCode}`,
    };

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.error("Error sending email:", error);
      } else {
        console.log("Email sent:", info.response);
      }
    });

    res
      .status(201)
      .json({
        message:
          "User created successfully. Check your email for verification.",
      });
  } catch (error) {
    res.status(500).json({ message: "An error occurred" });
  }
});

// Generate a random verification code
function generateVerificationCode() {
  const length = 6; // You can adjust the length of the verification code
  const characters = "0123456789";
  let code = "";
  for (let i = 0; i < length; i++) {
    code += characters[Math.floor(Math.random() * characters.length)];
  }
  return code;
}

module.exports = router;
