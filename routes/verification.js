const express = require("express");
const router = express.Router();
const User = require("../models/user");
const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "mwaflutterdeveloper@gmail.com",
    pass: "lbjiubhlpdvxhnrt",
  },
});

router.post("/", async (req, res) => {
  try {
    const { email, verificationCode } = req.body;

    // Find the user by email
    const user = await User.findOne({ email });

    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    // Compare verification code from the request with the stored verification code
    if (verificationCode !== user.verificationCode) {
      return res.status(401).json({ message: "Invalid verification code" });
    }

    // Mark the user's email as verified
    user.isEmailVerified = true;
    user.verificationCode = undefined; // Clear the verification code
    await user.save();

    // Send a welcome email to the user
    const mailOptions = {
      from: "mwaflutterdeveloper@gmail.com",
      to: email,
      subject: "Welcome to Your App",
      text: `Hello ${user.username}, welcome to Your App! We're glad to have you on board.`,
    };

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.error("Error sending email:", error);
      } else {
        console.log("Welcome email sent:", info.response);
      }
    });

    res.status(200).json({ message: "Email verification successful" });
  } catch (error) {
    console.error("Error:", error);
    res.status(500).json({ message: "An error occurred" });
  }
});

module.exports = router;
